const express = require("express");
const session = require("express-session");
const routes = require("./routes");
const { port, sessionValues } = require("./config/config");
const { logProfile } = require("./config/libraries/profiler");
const app = express();

const Redis = require("./config/database/Redis");

//Redis needs to be in async function
(async () => {
    const redis = new Redis(session);
    await redis.connect();
    sessionValues.store = redis.store;

    //

    app.use(session(sessionValues));
    app.use(express.json());
    app.use(express.urlencoded({ extended: true }));
    //profiler middleware
    app.use(logProfile);
    app.set("view engine", "ejs");
    app.use("/assets", express.static("assets"));
    app.use("/", routes);

    app.listen(port, () => console.log("Server stating at port:", port));
})();
