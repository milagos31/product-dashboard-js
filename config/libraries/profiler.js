const { profiler } = require("./../config");
class Profiler {
    logProfile(req, res, next) {
        const data = {
            alerts: "",
            method: "",
            post: [],
            query: "",
        };
        if (profiler) {
            data.method = req.method;
            for (const keyValue of Object.entries(req.session)) {
                if (keyValue.includes("warning") || keyValue.includes("success")) {
                    data.alerts = keyValue[1];
                }
            }
            for (const keyValue of Object.entries(req.body)) {
                data.post.push(`${keyValue[0]}: "${keyValue[1]}"`);
            }
            data.query = req.session.query;
            req.session.query = null;
            console.log(data);
        }
        req.session.profiler = { isEnabled: profiler, data };

        next();
    }
}

module.exports = new Profiler();
