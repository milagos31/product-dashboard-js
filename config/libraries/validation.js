class Validation {
    isEmail(email) {
        const split = email.split("@");

        //Check if email has valid parts such as "@", "." and cannot start "."
        if (split.length == 2 && split[0][0] != "." && split[0][split[0].length - 1] != ".") {
            const second = split[1].split(".");
            //Check if the first and second part has special characters
            if (second.length == 2 && this.isAlpha(second[1])) {
                const specials = `!#$%^&*()+= {}[]~;:'"|,<>?/\`\\`;
                let check = true;

                for (let i = 0; i < specials.length; i++) {
                    if (split[0].includes(specials[i]) || split[1].includes(specials[i])) {
                        check = false;
                        break;
                    }
                }
                if (check) {
                    return true;
                }
            }
        }

        return false;
    }

    isLength(str, min, max) {
        if (str.length >= min) {
            if (str.length <= max || max === undefined) {
                return true;
            }
        }

        return false;
    }

    matching(str1, str2) {
        return str1 === str2;
    }

    isAlpha(str) {
        const specials = `!#$%^&*()+={}[]~;:'"|,<>?/\`\\1234567890`;

        //Return false if it has numbers and special characters
        for (let i = 0; i < specials.length; i++) {
            if (str.includes(specials[i])) {
                return false;
            }
        }

        return str === str.trim() ? true : false;
    }
}

module.exports = new Validation();
