const { createClient } = require("redis");
const connectRedis = require("connect-redis");
const { redis } = require("./../config");

class Redis {
    constructor(session) {
        this.RedisStore = connectRedis(session);
        this.redisClient = createClient({ host: redis.host, port: redis.port, legacyMode: true });
        this.store = new this.RedisStore({ client: this.redisClient });
    }
    connect() {
        this.redisClient.connect();
        console.log("Session is now connected to Redis");
    }
}

module.exports = Redis;
