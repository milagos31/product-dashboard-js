const mysql = require("mysql");
const config = require("./../config");

class MySQL {
    constructor() {
        this.connection = mysql.createConnection({
            host: "localhost",
            user: config.mysql.user,
            password: config.mysql.password,
            database: "jstrack",
            connectTimeout: 3000,
            acquireTimeout: 3000,
            queueLimit: 0,
        });
        this.connection.connect();
        console.log(config);
    }
    dbQuery(query, values = []) {
        return new Promise((resolve) => {
            this.connection.query(query, values, (err, rows) => {
                if (err) throw err;
                resolve(rows);
            });
            // causing error when inserting new data
            // this.connection.end();
        });
    }
}

module.exports = MySQL;
