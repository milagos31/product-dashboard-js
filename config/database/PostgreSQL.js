const { postgres } = require("../config");
const pg = require("pg-promise")();

class PostgreSQL {
    constructor() {
        // "postgres://user:password@host:port/database"
        this.url = `postgres://${postgres.user}:${postgres.password}@${postgres.host}:${postgres.port}/${postgres.database}`;
        this.db = pg(this.url);
    }
    async dbQuery(query, values = []) {
        try {
            return await this.db.many(query, values);
        } catch (error) {
            return [];
        }
    }
}

module.exports = PostgreSQL;
