const fs = require("fs");
const YAML = require("js-yaml");
const raw = fs.readFileSync("./data.yaml");
const data = YAML.load(raw);

const config = {
    port: data.port,
    sessionValues: {
        secret: data.sessionValues.secret,
        resave: false,
        saveUninitialized: true,
        cookie: {
            maxAge: 1000 * 60 * 60 * 24, // 1 day
        },
    },
    mysql: {
        host: "localhost",
        user: "root",
        password: "root",
        database: "jstrack",
    },
    postgres: {
        // "postgres://user:password@host:port/database"
        user: data.pg.user,
        password: data.pg.password,
        host: "localhost",
        database: "product_dashboard",
        port: 5432,
    },
    redis: {
        host: "localhost",
        port: 6379,
        // legacyMode: true,
    },
    mongodb: {},
    profiler: false,
};

module.exports = config;
