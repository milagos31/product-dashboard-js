const Router = require("express").Router();
const UserController = require("./controllers/UserController");

//if user goes to index
Router.get("/", (req, res) => {
    res.redirect("/login");
});

//User
Router.use("/login", UserController.loginPage);
Router.use("/register", UserController.registerPage);

Router.post("/validate_login", UserController.validate_login);
Router.post("/validate_register", UserController.validate_register);

//Product

module.exports = Router;
