const PostgreSQL = require("../config/database/PostgreSQL");
const validate = require("../config/libraries/validation");
const md5 = require("md5");

class User extends PostgreSQL {
    async checkEmail(email, req) {
        const query = `SELECT * FROM users WHERE email = $1`;
        const result = await this.dbQuery(query, [email]);
        req.session.query = query;
        return result[0];
    }

    async addUser(req) {
        const { email, first_name, last_name, password } = req.body;
        const values = [email, first_name, last_name, md5(password)];
        const query = `INSERT INTO users (email, first_name, last_name, password) VALUES ($1,$2,$3,$4)`;
        const result = await this.dbQuery(query, values);
        req.session.query = query;
        return result;
    }

    async validateRegistration(req) {
        const { email, first_name, last_name, password, c_password } = req.body;
        let warning = [];
        //Check if email is valid or already existing
        if (!validate.isEmail(email)) {
            warning.push("Email is not valid!");
        } else {
            const existing = await this.checkEmail(email, req);
            existing && warning.push("Email already exists!");
        }
        //Check if names are alpha and with minimum characters
        if (!validate.isLength(first_name, 1) || !validate.isAlpha(first_name)) {
            warning.push("First Name is not valid!");
        }
        if (!validate.isLength(last_name, 2) || !validate.isAlpha(last_name)) {
            warning.push("Last Name is not valid!");
        }
        //Check if passwords has at least 8 characters and matches
        if (!validate.isLength(password, 8)) warning.push("Password should contain at least 8 characters!");
        if (!validate.matching(password, c_password)) warning.push("Password does not match!");

        if (warning.length == 0) {
            await this.addUser(req);
            return true;
        }

        return warning;
    }

    async validateLogin(req) {
        const { email, password } = req.body;

        const result = await this.checkEmail(email, req);
        if (result && result.password === md5(password)) {
            return result;
        }
        return "Invalid credentials!";
    }
}

module.exports = new User();
