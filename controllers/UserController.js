const User = require("../models/User");

class UserController {
    //redirect if '/'

    loginPage(req, res) {
        res.render("user/login");
    }
    registerPage(req, res) {
        res.render("user/register");
    }
    //validations
    async validate_login(req, res) {
        res.send(JSON.stringify(req.body));
    }

    async validate_register(req, res) {
        const isValid = await User.validateRegistration(req);
        console.log("isValid:", isValid);
        if (isValid === true) {
            const result = await User.addUser(req);
            console.log("result:", result);
        }
        res.send(JSON.stringify(req.body));
    }
}

module.exports = new UserController();
